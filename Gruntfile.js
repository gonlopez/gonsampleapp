module.exports = function (grunt) {
    
    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        clean: {
            build: ['dist/'],
            videoland: ['dist/build/app.js', 'dist/build/**.less']
        },

        less: {
            development: {
                options: {},
                files: {
                    "public_html/static/style/styles.css": "public_html/static/style/styles.less"
                }
            },
            production: {
                options: {
                    compress: true
                },
                files: {
                    "public_html/static/style/styles.css": "public_html/static/style/styles.less"
                }
            }
        },


        /**
         * Complexity validation of the javascript sources of this project.
         * https://www.npmjs.com/package/grunt-complexity
         * http://jscomplexity.org/complexity
         *
         * CYCLOMATIC COMPLEXITY
         * is the number of cycles as in a flow graph.
         * 10 to 20 is considered a moderate risk. Above 20 is considered high risk.
         *
         * HALSTEAD COMPLEXITY
         * Halstead metrics are based on definitions of operators and operands.
         * Difficulty
         *      (# distinct operators / 2) *
         *      (# operands / # distinct operands)
         * Volume
         *      (# operators + # operands) *
         *      log2(# distinct operators + # distinct operands)
         * Effort
         *      difficulty * volume
         *
         * MAINTAINABILITY INDEX
         * Invented by Oman and Hagemeister.
         * This is being calculated based on the halstead and cyclomatic complexity
         *  and the lines of code
         *
         * Oman and Hagemeister identified 65 as the threshold value below which a program should
         * be considered difficult to maintain.
         *
         * Please note that this is just an average value
         *  and not the best solution for maintainability stats.
         * It could just give an indication about possible places
         *  where maintainability could be harder.
         *
         * ADDITIONAL CONFIG VALUES FOR OUTPUTS:
         *      jsLintXML: 'report.xml',         // possible jsLint XML output
         *      checkstyleXML: 'checkstyle.xml', // possible style XML output
         *      pmdXML: 'pmd.xml',               // possible pmd XML output
         */
        complexity: {
            all: {
                src: ['./public_html/static/script/**/*.js'],
                exclude: ['./public_html/static/script/config.js'],
                options: {
                    breakOnErrors: false,
                    errorsOnly: false,                 // show warnings as well
                    cyclomatic: [15],                  // complexity of 15 (moderate risk)
                    halstead: [15],                    // complexity of 15 (moderate risk)
                    maintainability: 90,              // complexity of 15 (moderate risk)
                    hideComplexFunctions: false,
                    broadcast: false
                }
            }
        },

        watch: {
            files: [
                '**/*.less',
                '**/*.png'
            ],
            tasks: ['sprite:all', 'less:development']
        },

        sprite:{
            all: {
                src: 'public_html/static/style/assets/img/icons/sprites/**/*.png',
                dest: 'public_html/static/style/assets/img/spritesheet.png',
                destCss: 'public_html/static/style/less/sprites.css',
                imgPath: 'assets/img/spritesheet.png'
            }
        },

        eslint: {
            all: ['public_html/static/script/appui/**/*.js'],
            options: {
                config: '.eslintrc'
            }
        },

        concat: {
            build: {
                files: {
                    'dist/build/<%= grunt.task.current.args[0] %>/app.js' : [
                        '<%= appcorePath %>/src/core/require.js',
                        '<%= appcorePath %>/src/core/i18n.js',
                        '<%= appcorePath %>/src/tal/**/!(require).js',
                        '<%= appcorePath %>/src/rofl/**/!(crypto-sha256).js',
                        'public_html/static/script/**/!(app).js',
                        'public_html/static/script/app.js'
                    ]
                }
            }
        },

        copy: {
            build: {
                files: [
                    { expand: true, cwd: './public_html/static/script', src: ['config.js'], dest: 'dist/build/<%= grunt.task.current.args[0] %>/static/script/' },
                    { expand: true, cwd: './public_html/static/', src: ['style/*.css'], dest: 'dist/build/<%= grunt.task.current.args[0] %>/static/' },
                    { expand: true, cwd: './public_html/static/', src: ['style/assets/**'], dest: 'dist/build/<%= grunt.task.current.args[0] %>/static/' },
                    { expand: true, cwd: './public_html/appcore/Rofl/config/', src: ['Identifier.php'], dest: 'dist/' },
                    { expand: true, cwd: './platform-files/production/', src: ['index.php'], dest: 'dist/' },
                    { expand: true, cwd: './', src: ['dist/build/app.min.js'], dest: 'dist/build/<%= grunt.task.current.args[0] %>/static/script/', rename: function(dest, src) {
                        return dest + 'app.min.js';
                    } }
                ]
            }
        },

        jscrambler: {
            build : {
                options : {
                    keys : {
                        accessKey: '8D05C6A48404E109FBBC3CFE9A6BCA7EC250F678',
                        secretKey: 'A6743D5C7C35EB9EB5F17D5126099A096F4D1E51'
                    },
                    params : {
                        mode: 'html5',
                        rename_local: '%DEFAULT%',
                        whitespace: '%DEFAULT%',
                        function_outlining: '%DEFAULT%',
                        constant_folding: '%DEFAULT%'
                    }
                },
                files : [
                    {
                        src: ['dist/build/<%= grunt.task.current.args[0] %>/app.js'],
                        dest: 'dist/build/<%= grunt.task.current.args[0] %>/app.min.js'
                    }
                ]
            }
        },

        yuidoc: {
            compile: {
                name: '<%= pkg.name %>',
                description: '<%= pkg.description %>',
                version: '<%= pkg.version %>',
                url: '<%= pkg.homepage %>',
                options: {
                    paths: 'public_html/static/script/lib',
                    themedir: 'path/to/custom/theme/',
                    outdir: 'docs/'
                }
            }
        }
    });
    
    // Load the plugin that provides the "uglify" task.
    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);
    
    // Default taks(s).
    grunt.registerTask('default', [
        'sprite:all',
        'less:production',
        'watch'
    ]);

    // Default tasks - production build
    grunt.registerTask('build', ['clean:build', 'buildDevices']);

    grunt.registerTask('buildDevices', function() {

        var buildDevices = grunt.file.readJSON('platform-files/production/build-devices.json'),
            manufacturers = buildDevices.manufacturers,
            manufacturer,
            appcorePath = grunt.option('appcore') || 'public_html/appcore',
            pageStrategyElement = function(strategy, element, defaultValue) {
                if(!grunt.file.exists(appcorePath + '/Rofl/config/pagestrategy/' + strategy + '/' + element))
                    return defaultValue;

                return grunt.file.read(appcorePath + '/Rofl/config/pagestrategy/' + strategy + '/' + element);
            };
        grunt.config.set('appcorePath', appcorePath);

        grunt.task.run('concat:build:' + '');
        grunt.task.run('jscrambler:build:' + '');

        for (manufacturer in manufacturers) {
            var devices = manufacturers[manufacturer],
                length = devices.length,
                i;
            for (i = 0; i < length; ++i) {
                grunt.config.set('path', devices[i]);
                var index = grunt.file.read('platform-files/production/build.php'),
                    device = devices[i].split('-'),
                    configuration = grunt.file.readJSON(appcorePath + '/Rofl/config/devices/' + devices[i] + '.json');

                index = grunt.template.process(index, {
                    data: {
                        deviceConfiguration: JSON.stringify(configuration),
                        mimetype: pageStrategyElement(configuration.pageStrategy, 'mimetype', 'text/html;charset="UTF-8";'),
                        doctype: pageStrategyElement(configuration.pageStrategy, 'doctype', '<!doctype html>'),
                        root: pageStrategyElement(configuration.pageStrategy, 'rootelement', '<html>'),
                        headers: pageStrategyElement(configuration.pageStrategy, 'header', ''),
                        body: pageStrategyElement(configuration.pageStrategy, 'body', ''),
                        brand : device[0],
                        model : device[1],
                        path: devices[i]
                    }
                });
                index = index.replace(/\%application\%/g, 'application');
                grunt.file.write('dist/build/' + grunt.config.get('path') + '/index.php', index);

                grunt.task.run('copy:build:' + devices[i]);
            }
        }

        // TODO: Implement copy task for the platform specific files
        // that will be: Samsung (maple, tizen and horizon), Philips and Panasonic.
        //grunt.task.run('copy:samsungmaple');

        grunt.task.run('clean:videoland');
    });
    
};
