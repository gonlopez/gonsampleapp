<?php

    $javascript_cache_busting = false;

    header('Content-Type: <%= mimetype %>');
    echo ('<%= doctype %>');
?>
<%= root %>
<head>
    <%= headers %>
    <script src="static/script/app.min.js<?php if ($javascript_cache_busting): echo '?' . time(); endif; ?>"></script>
    <script type="text/javascript" src="static/script/config.js<?php if ($javascript_cache_busting): echo '?' . time(); endif; ?>"></script>
    <script>
        // <![CDATA[
        var antie = {
                framework: {
                    deviceConfiguration: <%= deviceConfiguration %>
            },
            applicationConfiguration: {
            id: 'videoland',
                deviceConfigName: '<%= path %>',
                baseUrl: './build/<%= path %>/'
        }
        };
        //]]>
    </script>
    <link rel="stylesheet" href="static/style/styles.css<?php if ($javascript_cache_busting): echo '?' . time(); endif; ?>"/>
</head>
<body>
    <%= body %>

    <!-- preload fonts -->
    <!--<div id="fontpreload1" class="fontPreload font-bold">lm</div>-->

    <!-- Create a loading message -->
    <div id="static-loading-screen" style="position: absolute; width: 100%; height: 100%; background: #000;">
        Application is loading...
    </div>

    <!-- Create a div to house the app -->
    <div id="app" class=""></div>

    <script type="text/javascript">
        // <![CDATA[

        // Deeplink stuff
//        if (deeplink) {
//            document.getElementById('loading-icon').className += ' ' + deeplink;
//        }
//        var hashParts = window.location.hash.split('/');
//        var deeplink = false;
//        if (hashParts.length > 3) {
//            if (hashParts[1] == 'channel' && hashParts[2] == 'id') {
//                deeplink = hashParts[3];
//            }
//        }

        require.config({
            config: {
                i18n: {
                    locale: 'nl-nl'
                }
            },
            priority: [],
            <?php if($javascript_cache_busting): ?>
                urlArgs: "bust=" +  (new Date()).getTime(),
            <?php endif; ?>
            callback: function() {}
        });

        var settings = {
            model: '<%= model %>',
            brand: '<%= brand %>',
            appid: 'videoland'
        };

        require(['application/config'],
            function(Config) {

                require(
                    [
                        'application/app'
                    ],
                    function(Application) {
                        function onReady() {
                            var staticLoadingScreen = document.getElementById('static-loading-screen');
                            staticLoadingScreen.parentNode.removeChild(staticLoadingScreen);
                        };
                        new Application(
                            document.getElementById('app'),
                            'static/style/',
                            'static/img/',
                            onReady
                        );
                    });
            });

        //]]>
    </script>
</body>
</html>