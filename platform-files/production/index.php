<?php

    require('Identifier.php');

    $identifier = new Config_Identifier();
    // Normalises to lower case with spaces replaced by underscores
    $device_brand = str_replace(' ', '_', strtolower($identifier->getBrand()));
    $device_model = str_replace(' ', '_', strtolower($identifier->getModel()));

    $environment = 'default';  // In production : default

    $device_config_path = 'build/' . $device_brand . "-" . $device_model . '-' . $environment;

    $app_dir = $device_config_path . '/index.php';

    // Check if folder corresponding to the device configuration exists
    if (file_exists($app_dir)) {
        header("Location: " . $app_dir);
    } else {
        if (is_dir($device_config_path)) {
            die('Path to the build directory ' . $device_config_path . ' is valid, but theres no index.php found.');
        }
        die('Path to the build directory was not found. ' . $app_dir);
    }
