<?php
/**
 * The entry point for TAL based applications
 *
 * @copyright Copyright (c) 2013 British Broadcasting Corporation
 * (http://www.bbc.co.uk) and TAL Contributors (1)
 *
 * (1) TAL Contributors are listed in the AUTHORS file and at
 *     https://github.com/fmtvp/TAL/AUTHORS - please extend this file,
 *     not this notice.
 *
 * @license Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * All rights reserved
 * Please contact us for an alternative licence
 */

// INIT AND CONFIG LOAD
defined('ANTIE_PATH') || define('ANTIE_PATH', './appcore/Rofl');

// Check TAL is available
if (!file_exists(ANTIE_PATH . '/antieframework.php')) {
    echo "<h2>Framework error</h2>";
    echo "<h4>antieframework.php can not be found.</h4>";
    echo "<h4>Please install the AppCore to a folder 'appcore' in your application's root</h4>";
    exit;
}

require(ANTIE_PATH . '/antieframework.php');

// Detect which device is using the application.
require(ANTIE_PATH . '/config/Identifier.php');

// Enable javascript cache busting (this should be disabled in production mode)
$javascript_cache_busting = true;

// Set up application ID and path to framework configuration directory
$application_id = "videoland";
$antie_config_path = ANTIE_PATH . '/config';

// Create an AntieFramework instance
$antie = new AntieFramework($antie_config_path);

// Create an Identifier instance used to get the brand and model of the device
// using the application
$identifier = new Config_Identifier();

// Get brand and model from url parameters, or use
// brand = default, model = webkit
// Normalises to lower case with spaces replaced by underscores
$device_brand = $antie->normaliseKeyNames($identifier->getBrand());
$device_model = $antie->normaliseKeyNames($identifier->getModel());
$environment = 'development';  // In production : default

// override device brand and model through url params
if (isset($_GET['brand']) && isset($_GET['model'])) {
    $device_brand = $_GET['brand'];
    $device_model = $_GET['model'];
}

// Framework device config files in format BRAND-MODEL-default.json
// Construct filename from this and config path
$device_configuration_name = $device_brand . "-" . $device_model . '-' . $environment;
$device_configuration_file_path = $antie_config_path . "/devices/" . $device_configuration_name . ".json";

// Load in device configuration
try {
    $device_configuration = @file_get_contents($device_configuration_file_path);
    if(!$device_configuration)
        throw new Exception("Device ($device_configuration_name) not supported");
} catch(Exception $e){
    echo $e->getMessage(); exit;
}

// Substitute appid wherever /%applicaion%/ is present in device configuration
$device_configuration = preg_replace('/%application%/m', 'application', $device_configuration);

// Decode to php object
$device_configuration_decoded = json_decode($device_configuration);

// PAGE GENERATION

// Set document mime type
header("Content-Type: " . $antie->getMimeType($device_configuration_decoded));

// Set doctype and opening html tag
echo $antie->getDocType($device_configuration_decoded);
echo $antie->getRootHtmlTag($device_configuration_decoded);
?>

<!-- HEAD -->

<head>
    <!-- Device specific head block (API loading etc) -->
<?php
    echo $antie->getDeviceHeaders($device_configuration_decoded);
    ?>

    <!-- Set up require aliases -->
    <script type="text/javascript">
    // <![CDATA[
        var require = {
            config: {
                i18n: {
                    locale: 'en-EN'
                }
            },
            baseUrl: "",
            paths: {
                application: 'static/script',
                antie: "./appcore/src/tal",
                core: "./appcore/src/core",
                rofl: "./appcore/src/rofl",
                local: ".."
            },
            priority: [],
//            <?php if($javascript_cache_busting): ?>
//                urlArgs: "bust=" +  (new Date()).getTime(),
//            <?php endif; ?>
            callback: function() {}
        };
    //]]>
    </script>

    <!-- Load require.js -->
    <script type="text/javascript" src="./appcore/src/core/require.js<?php if ($javascript_cache_busting): echo '?' . time(); endif; ?>"></script>

    <!-- Load application base style sheet -->
    <link rel="stylesheet" href="static/style/styles.css<?php if ($javascript_cache_busting): echo '?' . time(); endif; ?>"/>

    <!-- Expose device config to framework -->
    <script type="text/javascript">
    // <![CDATA[
        var antie = {
            framework: {
                deviceConfiguration: <?php echo $device_configuration; ?>
            },
            applicationConfiguration: {
                id: '<?php echo $application_id; ?>',
                deviceConfigName: '<?php echo $device_configuration_name; ?>'
            }
        };
    //]]>
    </script>

</head>



<!-- BODY -->

<body>

<!-- Add in device specific body (Plugins etc) -->
<?php echo $antie->getDeviceBody($device_configuration_decoded); ?>

<!-- preload fonts -->
<!--<div id="fontpreload1" class="fontPreload">abcd</div>-->

<!-- Create a loading message -->
<div id="static-loading-screen" style="position: absolute; width: 100%; height: 100%; background: #000;">
    Application is loading...
</div>

<!-- Create a div to house the app -->
<div id="app" class=""></div>

<!-- Load the application and launch, remove loading screen via callback -->
<script type='text/javascript'>
// <![CDATA[
require(['application/config'], function (Config) {
    require(['application/app'], function(
        App
    ) {
        'use strict';

        var onReady = function () {
            var staticLoadingScreen = document.getElementById('static-loading-screen');
            staticLoadingScreen.parentNode.removeChild(staticLoadingScreen);
        };

        new App(
            document.getElementById('app'),
            'static/style/',
            'static/style/assets/img/',
            onReady
        );
    });
});
//]]>
</script>

</body>
</html>