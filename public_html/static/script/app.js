define('application/app', [
    'rofl/application',
    'antie/runtimecontext',
    'rofl/widgets/container',
    'rofl/router',
    'rofl/widgets/loader',
    'core/i18n!application/nls/en-EN/ui'
], function (
    Application,
    RuntimeContext,
    Container,
    Router,
    Loader,
    Translator
) {
    'use strict';

    var appContainerElement,
        app,
        loader;

    return Application.extend({

        /**
         * The init method initializes the Application object, here it is only
         * used to call the method on rofl/application, but can be used in your
         * application for setting some initial application values
         * @param  {HTMLElement}   appDiv   The application element which should be handled
         * as root element
         * @param  {String}   styleDir The directory used for stylesheets
         * @param  {String}   imgDir   The directory used for images (can be used for preloading)
         * @param  {Function} callback Function to call when application.ready() is called
         * (usually used to destroy splash screens)
         */
        init: function (appDiv, styleDir, imgDir, callback) {
            this._super(appDiv, styleDir, imgDir, callback);
            appContainerElement = appDiv;

            this.setLogger();

            app = RuntimeContext.getCurrentApplication();

            // Initialize the loader
            //loader = new Loader('static/style/assets/img/loader.gif',
            //    {
            //        width: 135,
            //        height: 135
            //    },
            //    30
            //);
        },

        /**
         * Wrapper to use the device logger
         * @returns {*} Device logger
         */
        getDeviceLogger: function () {
            return this.getDevice().getLogger();
        },

        /**
         * Method to show a loader centered inside the given parent
         * @param {Widget} parent
         */
        showLoader: function (parent) {
            this.hideLoader();
            this.getRootWidget().addClass('loading');
            loader.show(parent || this.getRootWidget());
        },

        /**
         * Method to hide the current loader
         */
        hideLoader: function () {
            if (this.getRootWidget().hasClass('loading')) {
                this.getRootWidget().removeClass('loading');
                loader.hide();
            }
        },

        /**
         * Define Application's logger
         */
        setLogger: function () {
            var self = this,
                getLogger = function () {
                    return self.getDeviceLogger();
                };

            if (!window.system) {
                window.system = {
                    log: function () {
                        getLogger().log.apply({}, arguments);
                    },
                    debug: function () {
                        getLogger().debug.apply({}, arguments);
                    },
                    info: function () {
                        getLogger().info.apply({}, arguments);
                    },
                    warn: function () {
                        getLogger().warn.apply({}, arguments);
                    },
                    error: function () {
                        getLogger().error.apply({}, arguments);
                    }
                };
            }
        },

        /**
         * SetRootContainer is a method used solely for setting the root widget
         * for the application.
         */
        setRootContainer: function () {
            var container = new Container();

            container.outputElement = appContainerElement;
            this.setRootWidget(container);
        },

        /**
         * Run is the method used by the application after the application is
         * initialized, here we create our root widget (onto which everything is added)
         */
        run: function () {
            // set the root conainer of the app
            this.setRootContainer();

            // set the translator
            this.setTranslation(Translator);

            // Create our ComponentContainer (with the ID player), so we can
            // show components on this
            this.addComponentContainer('menu');
            this.addComponentContainer('main');
            this.addComponentContainer('player');
            this.addComponentContainer('modal');

            // notification at the app has started.
            this.ready();
        },

        /**
         * The application route method, this is executed by the application
         * on startup (_default route) and can be used to route to different
         * screens within the application
         * @param  {String|Array} path The route you want to used, defined in the config.
         * Can also be an array in the case of deeplinks
         * @param  {Object} opts The options object can contain information you want
         * to use in the route
         */
        route: function (path, opts) {
            var instance = Router.getInstance();

            if (typeof path === 'string') {
                instance.route(path, opts);
            } else {
                instance.deeplink(path);
            }
        }
    });
});
