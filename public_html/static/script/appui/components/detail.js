/**
 * A controller should manage all the logic of a view.
 * @description Detail: Component to display the detailed info of a film [Dependencies of component] (Instances of the
 * dependencies).
 * @extends Component
 * @author gonzalo lopez gonzalo.lopez@24i.com
 */
define('application/appui/components/detail', [
    'antie/widgets/component',
    'antie/widgets/verticallist',
    'antie/widgets/horizontallist',
    'antie/application',
    'rofl/widgets/label',
    'rofl/widgets/button',
    'rofl/widgets/image',
    'rofl/lib/y',
    'rofl/router',
    'rofl/datasources/api'
], function (
    Component,
    VerticalList,
    HorizontalList,
    Application,
    Label,
    Button,
    Image,
    Utils,
    Router,
    API
) {
    'use strict';

    return Component.extend({

        /**
         * @description Initializes the controller
         * @constructor
         * @public
         */
        init: function () {

            // Call to Component constructor, must be the first method call just after variables declaration
            this._super('detailsComponent');

            // Create static part of the view
            this._createView();

            // Set up the execution context to methods for life cycle events
            this._onBeforeRenderBound = Utils.bind(this._onBeforeRenderEvent, this);
            this._onBeforeShowBound = Utils.bind(this._onBeforeShowEvent, this);
            this._onBeforeHideBound = Utils.bind(this._onBeforeHideEvent, this);

            // Set up the execution context to methods for flow events
            this._onSelectBackEventBound = Utils.bind(this._onSelectBackEvent, this);
            this._onSelectPlayEventBound = Utils.bind(this._onSelectPlayEvent, this);

            // Set up the execution context to methods for API responses
            this._successResponseBound = Utils.bind(this._successResponse, this);

            // Set event listeners of the component life cycle
            this.addEventListener('beforerender', this._onBeforeRenderBound);
            this.addEventListener('beforeshow', this._onBeforeShowBound);
            this.addEventListener('beforehide', this._onBeforeHideBound);
        },

        /**
         * On before show handler method.
         * @private
         */
        _onBeforeShowEvent: function () {

            // Add listeners not related with life cycle of the widget
            this._btnBack.addEventListener('select', this._onSelectBackEventBound);
            this._btnPlay.addEventListener('select', this._onSelectPlayEventBound);
        },

        /**
         * On before render handler method.
         * @param {object} evt Event arguments.
         * @private
         */
        _onBeforeRenderEvent: function (evt) {
            var api = new API(),
                filmId = evt.args.filmid,
                params = { filmid : filmId};

            // Id of the film to get the detailed data.
            this._filmId = filmId;

            // API call to api defined in config called filmlist: With filmId parameter in the endpoint the server
            // returns the data about the specific film. detailModel is the model to parse the data.
            api
                .read('detailModel', { params : params })
                .then(
                    this._successResponseBound,
                    this._processError);
        },

        /**
         * On before hide handler method.
         * @private
         */
        _onBeforeHideEvent: function () {

            // Always remove the listeners not related to the widget life cycle
            this._btnBack.removeEventListener('select', this._onSelectBackEventBound);
            this._btnPlay.removeEventListener('select', this._onSelectPlayEventBound);
        },

        /**
         * On select back button event handler.
         * @private
         */
        _onSelectBackEvent: function () {

            // Go back one step in the browsing history
            this.parentWidget.back();
        },

        /**
         * On select play button event handler.
         * @private
         */
        _onSelectPlayEvent: function () {

            // Route to "play" component
            Router.getInstance().route('#play', { filmid: this._filmId });
        },

        /**
         * Create the static part of the view before call to API to get data.
         * @private
         */
        _createView: function(){

            // Create widgets
            var app = Application.getCurrentApplication(),
                verticalList = new VerticalList(),
                buttonBar = new HorizontalList(),
                btnBack = new Button('btnBack'),
                btnBackLabel = new Label('btnBackLabel', app.__('buttons.back')),
                titleLabel = new Label('', ''),
                coverImage = new Image('', ''),
                descriptionLabel = new Label('', ''),
                btnPlay = new Button('btnPlay'),
                btnPlayLabel = new Label('btnPlayLabel', 'Play');

            // Add styling to widgets
            titleLabel.addClass(['film-title']);

            // Insert widgets into their containers
            verticalList.appendChildWidget(titleLabel);
            verticalList.appendChildWidget(coverImage);
            verticalList.appendChildWidget(descriptionLabel);
            btnBack.appendChildWidget(btnBackLabel);
            buttonBar.appendChildWidget(btnBack);
            btnPlay.appendChildWidget(btnPlayLabel);
            buttonBar.appendChildWidget(btnPlay);
            verticalList.appendChildWidget(buttonBar);
            this.appendChildWidget(verticalList);

            // Expose the dynamic widgets
            this._titleLabel = titleLabel;
            this._coverImage = coverImage;
            this._descriptionLabel = descriptionLabel;
            this._btnBack = btnBack;
            this._btnPlay = btnPlay;
        },

        /**
         * Handle the successful response from the content endpoint {@link detailModel}.
         * @param {object} response Returned model from the API of detailModel.
         * @private
         */
        _successResponse: function (response) {
            var filmInfo = response;
            this._titleLabel.setText(filmInfo.name);
            this._coverImage.setSrc(filmInfo.coverUrl);
            this._descriptionLabel.setText(filmInfo.longDescription);
        },

        /**
         * Process an API error
         * @param {object} error API error object
         * @private
         */
        _processError: function(error){
            system.error(error.message);
        }
    });
});
