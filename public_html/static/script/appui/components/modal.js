/**
 * @description modal: Modal component to display a message or take a decision [Dependencies of component] (Instances
 * of the dependencies).
 * @extends Component
 * @author gonzalo lopez gonzalo.lopez@24i.com
 */
define('application/appui/components/modal', [
    'antie/widgets/component',
    'antie/widgets/verticallist',
    'rofl/widgets/label',
    'rofl/widgets/button',
    'rofl/lib/y'
], function(
    Component,
    VerticalList,
    Label,
    Button,
    utils
){
    'use strict';

    return Component.extend({
        /**
         * @description Initializes the controller
         * @constructor
         * @public
         */
        init: function () {

            // Call to Component constructor, must be the first method call just after variables declaration
            this._super('modalComponent');

            // Force modal
            this.setIsModal(true);

            // Create static part of the view
            this._createView();

            // Set up the execution context to methods for life cycle events
            this._onBeforeRenderBound = utils.bind(this._onBeforeRenderEvent, this);
            this._onBeforeShowBound = utils.bind(this._onBeforeShowEvent, this);
            this._onAfterShowBound = utils.bind(this._onAfterShowEvent, this);
            this._onBeforeHideBound = utils.bind(this._onBeforeHideEvent, this);

            // Set up the execution context to methods for flow events
            this._onSelectCloseEventBound = utils.bind(this._onSelectCloseEvent, this);

            // Set event listeners of the component life cycle
            this.addEventListener('beforerender', this._onBeforeRenderBound);
            this.addEventListener('aftershow', this._onAfterShowBound);
            this.addEventListener('beforeshow', this._onBeforeShowBound);
            this.addEventListener('beforehide', this._onBeforeHideBound);
        },

        /**
         * On before show handler method.
         * @private
         */
        _onBeforeShowEvent: function () {

            // Add listeners not related with life cycle of the widget
            this._btnClose.addEventListener('select', this._onSelectCloseEventBound);
        },

        /**
         * On before render handler method.
         * @private
         */
        _onBeforeRenderEvent: function () {

        },

        /**
         * On before hide handler method.
         * @private
         */
        _onBeforeHideEvent: function () {

            // Always remove the listeners not related to the widget life cycle
            this._btnClose.removeEventListener('select', this._onSelectCloseEventBound);
        },

        /**
         * On after show handler method.
         * @private
         */
        _onAfterShowEvent: function(){
            this._btnClose.focus();
        },

        /**
         * On select close button event handler.
         * @private
         */
        _onSelectCloseEvent: function () {

            // Go back one step in the browsing history
            this.parentWidget.back();
        },

        /**
         * Create the static part of the view before call to API to get data.
         * @private
         */
        _createView: function(){

            // Create widgets
            var verticalList = new VerticalList(),
                btnClose = new Button('btnClose'),
                btnCloseLabel = new Label('', 'Close'),
                contentLabel = new Label('', 'Test Modal Content'),
                titleLabel = new Label('', 'Test Modal Title');

            // Insert widgets into their containers
            btnClose.appendChildWidget(btnCloseLabel);
            verticalList.appendChildWidget(titleLabel);
            verticalList.appendChildWidget(contentLabel);
            verticalList.appendChildWidget(btnClose);
            this.appendChildWidget(verticalList);

            // Expose the dynamic widgets
            this._btnClose = btnClose;
        }
    });
});