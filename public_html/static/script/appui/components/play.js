/**
 * A controller should manage all the logic of a view.
 * @description play: Component to reproduce the selected film [Dependencies of component] (Instances of the
 * dependencies).
 * @extends Component
 * @author gonzalo lopez gonzalo.lopez@24i.com
 */
define('application/appui/components/play', [
    'antie/runtimecontext',
    'antie/devices/mediaplayer/mediaplayer',
    'antie/widgets/component',
    'antie/widgets/verticallist',
    'antie/widgets/horizontallist',
    'rofl/widgets/label',
    'rofl/widgets/button',
    'rofl/devices/device',
    'rofl/lib/y'
], function (
    RuntimeContext,
    MediaPlayer,
    Component,
    VerticalList,
    HorizontalList,
    Label,
    Button,
    Device,
    utils
) {
    'use strict';

    return Component.extend({

        /**
         * @description Initializes the controller
         * @constructor
         * @public
         */
        init: function () {

            // Device that is running the app.
            this._device = RuntimeContext.getDevice();

            // Call to Component constructor, must be the first method call just after variables declaration
            this._super('playComponent');

            // Create static part of the view
            this._createView();

            // Set up the execution context to methods for life cycle events
            this._onBeforeRenderBound = utils.bind(this._onBeforeRenderEvent, this);
            this._onBeforeShowBound = utils.bind(this._onBeforeShowEvent, this);
            this._onBeforeHideBound = utils.bind(this._onBeforeHideEvent, this);
            this._onAfterShowBound = utils.bind(this._onAfterShowEvent, this);

            // Set up the execution context to methods for flow events
            this._onSelectBackEventBound = utils.bind(this._onSelectBackEvent, this);

            // Set up the execution context to methods for API responses

            // Set event listeners of the component life cycle
            this.addEventListener('beforerender', this._onBeforeRenderBound);
            this.addEventListener('beforeshow', this._onBeforeShowBound);
            this.addEventListener('aftershow', this._onAfterShowBound);
            this.addEventListener('beforehide', this._onBeforeHideBound);
        },

        /**
         * On before show handler method.
         * @private
         */
        _onBeforeShowEvent: function () {

            // Media player creation and initialization
            this._player = this._device.getMediaPlayer();
            var source = 'http://desprotrailers.nubeox.com/trailer/nubeo-70267f663f806e8007b6150ee67866ba/cincuenta.mp4';
            this._player.setSource(MediaPlayer.TYPE.VIDEO, source, 'video/mp4');

            // Add listeners not related with life cycle of the widget
            this._btnBack.addEventListener('select', this._onSelectBackEventBound);
        },

        /**
         * On after show handler method.
         * @private
         */
        _onAfterShowEvent: function(){

            // Update of the media player status
            this._player.beginPlayback();

            // Play the film
            this._player.playFrom(0);
        },

        /**
         * On before render handler method.
         * @private
         */
        _onBeforeRenderEvent: function () {

        },

        /**
         * On before hide handler method.
         * @private
         */
        _onBeforeHideEvent: function () {

            // Always remove the listeners not related to the widget life cycle
            this._btnBack.removeEventListener('select', this._onSelectBackEventBound);

            // Always stop and reset (hide) the media player before leave the component
            this._player.stop();
            this._player.reset();
        },

        /**
         * On select back button event handler.
         * @private
         */
        _onSelectBackEvent: function () {

            // Go back one step in the browsing history
            this.parentWidget.back();
        },

        /**
         * Create the static part of the view before call to API to get data.
         * @private
         */
        _createView: function(){

            // Create widgets
            var verticalList = new VerticalList(),
                buttonBar = new HorizontalList(),
                btnBack = new Button('btnBack'),
                btnBackLabel = new Label('btnBackLabel', 'Back');

            // Add styling to widgets

            // Insert widgets into their containers
            btnBack.appendChildWidget(btnBackLabel);
            buttonBar.appendChildWidget(btnBack);
            verticalList.appendChildWidget(buttonBar);
            this.appendChildWidget(verticalList);

            // Expose the dynamic widgets
            this._btnBack = btnBack;
        }
    });
});
