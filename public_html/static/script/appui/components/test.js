/**
 * A controller should manage all the logic of a view.
 * @description Test: Component to display the list of films available Dependencies of component] (Instances of the
 * dependencies).
 * @extends Component
 * @author gonzalo lopez gonzalo.lopez@24i.com
 */
define('application/appui/components/test', [
    'antie/widgets/component',
    'antie/widgets/horizontallist',
    'antie/widgets/verticallist',
    'rofl/widgets/button',
    'rofl/widgets/label',
    'application/appui/widgets/CoverWidget',
    'rofl/lib/y',
    'rofl/router',
    'rofl/datasources/api'
], function (Component,
             HorizontalList,
             VerticalList,
             Button,
             Label,
             CoverWidget,
             utils,
             Router,
             API
) {
    'use strict';

    return Component.extend({
        /**
         * @description Horizontal list container for the covers
         */
        _horizontalList: undefined,

        /**
         * @description Initializes the controller
         * @constructor
         * @public
         */
        init: function () {

            // Call to Component constructor, must be the first method call just after variables declaration
            this._super('test');

            // Create static part of the view
            this._createView();

            // Set up the execution context to methods for life cycle events
            this._onBeforeRenderBound = utils.bind(this._onBeforeRenderEvent, this);
            this._onBeforeShowBound = utils.bind(this._onBeforeShowEvent, this);
            this._onAfterShowBound = utils.bind(this._onAfterShowEvent, this);

            // Set up the execution context to methods for API responses
            this._contentSuccessResponseBound = utils.bind(this._contentSuccessResponse, this);

            // Set event listeners of the component life cycle
            this.addEventListener('beforerender', this._onBeforeRenderBound);
            this.addEventListener('beforeshow', this._onBeforeShowBound);
            this.addEventListener('aftershow', this._onAfterShowBound);
        },

        /**
         * On before show handler method.
         * @private
         */
        _onBeforeShowEvent: function () {

            // Add listeners not related with life cycle of the widget
            this._btnModal.addEventListener('select', this._onSelectModalEvent);
        },

        /**
         * On before render handler method.
         * @param {object} args Arguments from the router call.
         * @private
         */
        _onBeforeRenderEvent: function (args) {
            // Get films data from the default api
            var api = new API();
            if(args.fromBack){
                return;
            }
            api
                .read('contentModel')
                .then(
                    this._contentSuccessResponseBound,
                    this._processError);
        },

        /**
         * On after show handler method.
         * @param {object} args parameters of the call
         * @private
         */
        _onAfterShowEvent: function(args){

            // If the call comes back from other view in the flow, remove and reload everything
            if(args.fromBack){
                return;
            }

            // Add delegated listener on the list of films
            this._horizontalList.addEventListener('select', this._onSelectBound);
        },

        /**
         * Handle the successful response from the content endpoint
         * {@link 'application/appui/datasources/models/contentModel'}.
         * @param {object} response Parsed response from the API
         * @private
         */
        _contentSuccessResponse: function(response){
            var self = this;

            // Create containers for every item (depends on the view layout)
            utils.each(response, function(item){
               self._horizontalList.appendChildWidget(new CoverWidget(item));
            });
        },

        /**
         * Process an API error
         * @param {object} error API error
         * @private
         */
        _processError: function(error){
            system.error(error.message);
        },

        /**
         * Create the static part of the view before call to API to get data.
         * @private
         */
        _createView: function(){
            var horizontalList = new HorizontalList(),
                modalBtnLabel = new Label('', 'Open Modal'),
                modalBtn = new Button('');

            modalBtn.appendChildWidget(modalBtnLabel);
            this.appendChildWidget(horizontalList);
            this.appendChildWidget(modalBtn);

            // Expose the widgets
            this._horizontalList = horizontalList;
            this._btnModal = modalBtn;
        },

        /**
         * On select handler method on the list of films.
         * @param {object} evt Event select
         * @private
         */
        _onSelectBound: function(evt){

            // The app is redirected to detail component
            Router.getInstance().route('#detail', { filmid: evt.target._dataItem.id });

            // In order to stop the bubbling of the event from the detail widget
            evt.stopPropagation();
        },

        /**
         * On select handler method on the modal button.
         * @private
         */
        _onSelectModalEvent: function(){

            // Open the modal component
            Router.getInstance().route('#modal');
        }
    });
});
