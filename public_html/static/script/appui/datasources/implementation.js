define('application/appui/datasources/implementation', [
    'rofl/datasources/abstract',
    'rofl/datasources/adapter/rest'
], function(Abstract, RestAdapter) {

    return Abstract.extend({

        init: function (config) {
            this.setModelPath(config.modelPath);
            this.setApiEndpoint(config.endpoint.url);
            this.setAdapter(new RestAdapter());
        }

    });

});