/**
 * A model should parse and transform the data that the API provides
 * @description contentModel: Model to abstract the data of list films [Dependencies of component] (Instances of the
 * dependencies).
 * @extends Abstract
 * @author gonzalo lopez gonzalo.lopez@24i.com
 */
define('application/appui/datasources/models/contentModel', [
    'rofl/models/api/abstract',
    'rofl/lib/y'
], function (
    Abstract,
    utils
) {
    'use strict';

    return Abstract.extend({

        /**
         * @description Initializes the model
         * @constructor
         * @public
         */
        init: function () {
        },

        /**
         * @description Build the endpoint of the url to make the API call. This method is used in the abstract
         * datasource.
         * @param {object} params Arguments needed to build the endpoint text
         * @returns {string} endpoint Built endpoint
         * @public
         */
        resolveEndpoint: function (params) {
            var endpoint = 'combined/series';

            if (typeof params !== 'undefined') {
                endpoint += '?' + utils.serialize(params);
            }

            return endpoint;
        },

        /**
         * @description Validate the data returned in the response. You should check whatever you need to get from
         * the call.
         * @param {object} response Data to validate
         * @returns {string} endpoint Built endpoint
         * @public
         */
        validateResponse: function(response){

            // Just to test something
            return response[1].results;
        },

        /**
         * @description Transform the data returned by the provider into an specific model
         * @param {object} data film list data in provider's format.
         * @returns {Array} model Array of films already parsed.
         */
        transformFrom: function (data) {
            var self = this,
                model = [],
                film;

            utils.each(data[1].results, function(item){
                film = {};
                film.id = item['id'];
                film.name = item['name'];
                film.cover = self._getCover(item['poster']);
                model.push(film);
            });

            return model;
        },

        /**
         * Stub method for getting a transformed property
         * @returns {String|Boolean} Either the value or False if not set
         */
        getModelProperty: function () {
            return this.modelProperty || false;
        },

        /**
         * @description Get the final url to get the cover in the proper size
         * @param {String} coverUrl Url to the resource without dimensions.
         * @returns {String} url to the resource with dimensions.
         */
        _getCover: function(coverUrl) {
            var format = {
                width: '166',
                height: '248'
            }
            return coverUrl.replace('[format]', format['width'] + 'x' + format['height']);
        }
    });

});