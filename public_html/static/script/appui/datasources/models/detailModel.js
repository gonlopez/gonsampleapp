/**
 * A model should parse and transform the data that the API provides.
 * @description detailModel: Model to abstract the data of a single film [Dependencies of component] (Instances of the
 * dependencies).
 * @extends Abstract
 * @author gonzalo lopez gonzalo.lopez@24i.com
 */
define('application/appui/datasources/models/detailModel', [
    'rofl/models/api/abstract'
], function (
    Abstract
) {
    'use strict';

    return Abstract.extend({

        /**
         * @description Initializes the model
         * @constructor
         * @public
         */
        init: function () {

        },

        /**
         * @description Build the endpoint of the url to make the API call. This method is used in the abstract
         * datasource.
         * @param {object} params Arguments needed to build the endpoint text
         * @returns {string} endpoint Built endpoint
         * @public
         */
        resolveEndpoint: function (params) {

            // To get the information about an specific film we need to append the film id in the url
            var endpoint = 'combined/detail/' + params.filmid + '/serie/' || '';
            return endpoint;
        },

        /**
         * @description Validate the data returned in the response. You should check whatever you need to get from
         * the call.
         * @param {object} response Data to validate
         * @returns {string} endpoint Built endpoint
         * @public
         */
        validateResponse: function(response){

            // Just to test something
            return response;
        },

        /**
         * @description Transform the data returned by the provider into an specific model
         * @param {object} data film data in provider's format.
         * @returns {object} film Data of the film already parsed.
         */
        transformFrom: function (data) {

            // Private model built
            var film = {};
                film.id = data['id'];
                film.name = data['name'];
                film.coverUrl = this._getCover(data['poster']);
                film.longDescription = data['full_text'];
            return film;
        },

        /**
         * Stub method for getting a transformed property
         * @returns {String|Boolean} Either the value or False if not set
         */
        getModelProperty: function () {
            return this.modelProperty || false;
        },

        /**
         * @description Get the final url to get the cover in the proper size
         * @param {String} coverUrl Url to the resource without dimensions.
         * @returns {String} url to the resource with dimensions.
         */
        _getCover: function(coverUrl) {
            var format = {
                width: '166',
                height: '248'
            };
            return coverUrl.replace('[format]', format['width'] + 'x' + format['height']);
        }
    });
});