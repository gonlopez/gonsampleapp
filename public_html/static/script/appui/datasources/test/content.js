require('datasources/test/content',
    [
        'rofl/datasources/abstract'
    ], function (Abstract) {
        return Abstract.extend({
            init: function(){

            },
            getApiEndpoint: function () {
                return 'content';
            }
        });
    });