define('application/appui/layouts/720p',
    {
        images: {
            path: '/static/style/assets/img/'
        },
        classes: [
            'layout720p'
        ],
        css: [],
        requiredScreenSize: {
            width: 1280,
            height: 720
        },
        textPager: {
            lineHeight: 25
        }
    }
);
