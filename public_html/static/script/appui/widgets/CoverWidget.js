/**
 * A private widget should be a group of subwidgets with a simple behaviour.
 * @description Cover widget for test application. Contains an image and a label for a film.
 * @extends Button widget
 * @author gonzalo lopez gonzalo.lopez@24i.com
 */
define('application/appui/widgets/CoverWidget', [
    'rofl/widgets/button',
    'rofl/widgets/image',
    'rofl/widgets/label',
    'rofl/lib/y'
], function(
    Button,
    Image,
    Label,
    utils
){
    'use strict';

    return Button.extend({

        /**
         * @description Initialization for the cover widget
         * @param {object} obj Film data to build the widget
         * @constructor
         */
        init: function(obj){

            // Call to Component constructor, must be the first method call just after variables declaration
            this._super();

            // Bind the object to the widget to allow getting the info in events
            this.setDataItem(obj);

            // Append the sub-widgets inside the container for every film
            this.appendChildWidget(new Image('', obj.cover));
            this.appendChildWidget(new Label('', obj.name));

            // Set up the execution context to methods for life cycle events
            this._onBeforeRenderBound = utils.bind(this._onBeforeRenderEvent, this);
            this._onBeforeShowBound = utils.bind(this._onBeforeShowEvent, this);
            this._onBeforeHideBound = utils.bind(this._onBeforeHideEvent, this);

            // Set event listeners of the component life cycle
            this.addEventListener('beforerender', this._onBeforeRenderBound);
            this.addEventListener('beforeshow', this._onBeforeShowBound);
            this.addEventListener('beforehide', this._onBeforeHideBound);
        },

        _onBeforeShowEvent: function () {},
        _onBeforeRenderEvent: function () {},
        _onBeforeHideEvent: function () {}
    });
});
