define('application/config', [
    'rofl/config',
    'rofl/lib/object.extend'
], function (Config) {
    'use strict';

    var baseComponentsPath = 'application/appui/components',
        configuration = {
            baseUrl: './',
            route: {

                /**
                 * This is the default route being loaded by the application.
                 * a route can contain the following properties or an array
                 * containing objects with these properties:
                 *     - method (either push or show, these relate to application.push/showComponent)
                 *     - container (the ComponentContainer to show the component on)
                 *     - component (the path to the component you want to show)
                 */
                '_default': [{
                    method: 'push',
                    container: 'main',
                    component: baseComponentsPath + '/test'
                }
                ],

                /**
                 * Then the rest of different routes of the application
                 * detail shows the information of a selected film
                 */
                '#detail': [{
                    method: 'push',
                    container: 'main',
                    component: baseComponentsPath + '/detail'
                }
                ],

                /**
                 * play reproduce the selected film
                 */
                '#play': [{
                    method: 'push',
                    container: 'main',
                    component: baseComponentsPath + '/play'
                }
                ],

                '#modal': [{
                    method: 'show',
                    container: 'modal',
                    component: baseComponentsPath + '/modal'
                }]
            },

            api: {

                /**
                 *  This is the default API source. An api source can contain the following properties or an array
                 *  containing objects with these properties:
                 *      - datasource (path to the basic configuration)
                 *      - endpoint: (object with an url that point to the resource)
                 *      - modelPath: (path to the model that handles the raw data obtained from the endpoint)
                 */
                _default: 'filmlist',
                filmlist: {
                    datasource: 'application/appui/datasources/implementation',
                    endpoint: {
                        url: './api/'
                    },
                    headers: {
                        'videoland-platform': 'videoland.com',
                        'videoland-auth-token': 'd1f63d16cf3a37f2da4c191a7b922912c7444c2d',
                        'Accept': 'application/vnd.client.v1+json',
                        'Content-Type': 'application/json'

                    },
                    modelPath: 'application/appui/datasources/models/'
                }
            }
        };


    antie.applicationConfiguration = Object.deepExtend(
        antie.applicationConfiguration,
        configuration
    );

    return Config;
})
;
